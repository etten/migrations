# Etten\Migrations

Based on **[Nextras\Migrations](https://github.com/nextras/migrations)**.

**Supported databases:**
* PostgreSQL
* MySQL

**Supported DBALs:**
* [Nette Database](https://github.com/nette/database)
* [Doctrine DBAL](https://github.com/doctrine/dbal)
* [dibi](https://github.com/dg/dibi)


License
-------

*Based on [Nextras\Migrations](https://github.com/nextras/migrations) and further customized.*
*Based on [Clevis\Migration](https://github.com/clevis/migration) by Petr Procházka and further improved.*

New BSD License. See full [license](license.md).
