<?php

/**
 * This file is part of etten/migrations.
 * Copyright © 2016 Jaroslav Hranička <hranicka@outlook.com>
 */

namespace Etten\Migrations;

class LogicException extends \LogicException implements Exception
{

}
