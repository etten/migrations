<?php

namespace Etten\Migrations\Engine;

class SystemSetup
{

    public function __invoke()
    {
        @set_time_limit(86400);
        @ini_set('memory_limit', '2G');
    }

}
