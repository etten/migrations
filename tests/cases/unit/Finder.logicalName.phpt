<?php

/**
 * @testCase
 */

namespace Etten\Migrations;

use Etten\Migrations\Engine\FileFactory;
use Etten\Migrations\Engine\Finder;
use Etten\Migrations\Entities\Group;
use Mockery;
use Tester;
use Tester\Assert;

require __DIR__ . '/../../bootstrap.php';

class FinderLogicalNameTest extends Tester\TestCase
{

	/** @var FileFactory */
	private $fileFactory;

	/** @var Finder|Mockery\MockInterface */
	private $finder;

	/** @var Group[] */
	private $groups;

	protected function setUp()
	{
		parent::setUp();

		$this->fileFactory = Mockery::mock(FileFactory::class, [['sql']])
			->shouldAllowMockingProtectedMethods()
			->shouldDeferMissing()
			->shouldReceive('getChecksum')
			->getMock();

		$this->finder = Mockery::mock(Finder::class, [$this->fileFactory])
			->shouldAllowMockingProtectedMethods()
			->shouldDeferMissing();

		$group = new Group();
		$group->dependencies = [];
		$group->directory = './baseDir/structures';
		$group->enabled = TRUE;
		$group->name = 'structures';
		$this->groups = [$group];
	}

	public function testSimple()
	{
		$this->finder->shouldReceive('getItems')
			->with('./baseDir/structures')
			->andReturn([
				'2015-03-04.sql',
				'2015-03-06.sql',
				'2015-07-06.sql',
			]);

		$files = $this->finder->find($this->groups);
		Assert::count(3, $files);
		Assert::same('2015-03-04.sql', $files[0]->name);
		Assert::same('2015-03-06.sql', $files[1]->name);
		Assert::same('2015-07-06.sql', $files[2]->name);
	}

	public function testComplex()
	{
		$this->finder->shouldReceive('getItems')
			->with('./baseDir/structures')
			->andReturn(['2015', '2015-07-06.sql']);

		$this->finder->shouldReceive('getItems')
			->with('./baseDir/structures/2015')
			->andReturn(['03', '03-06.sql']);

		$this->finder->shouldReceive('getItems')
			->with('./baseDir/structures/2015/03')
			->andReturn(['2015-03-04.sql']);

		$files = $this->finder->find($this->groups);
		Assert::count(3, $files);
		Assert::same('2015-07-06.sql', $files[0]->name);
		Assert::same('2015-03-06.sql', $files[1]->name);
		Assert::same('2015-03-04.sql', $files[2]->name);
	}

}

$test = new FinderLogicalNameTest();
$test->run();
